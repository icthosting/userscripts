# Hosting Userscripts

## Install Userscript Managers

To use these scripts install a userscript handler for your browser.

Either:

- [Grease Monkey] for Firefox.
- [Tamper Monkey] for Chrome.

[Grease Monkey]: https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/
[Tamper Monkey]: https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo

## Installing Userscripts

Click the links to install the user scripts or source to view the source.

- [Track-It Ticket Linker] - [Source](trackit-linker.user.js)

[Track-It Ticket Linker]: https://trackit.sydney.edu.au/userscripts/trackit-linker.user.js
