// ==UserScript==
// @name         Track-It Ticket Linker
// @namespace    https://bitbucket.org/icthosting/userscripts/
// @version      0.2.1
// @description  Finds instances of Service Now tickets and converts them into links.
// @author       Stuart Esdaile <stuart.esdaile@sydney.edu.au> (https://bitbucket.org/stuart-esdaile/)
// @copyright    2016+, The University of Sydney (http://sydney.edu.au/)
// @license      GPL version 3 or any later version; http://www.gnu.org/copyleft/gpl.html
// @downloadURL  https://bitbucket.org/icthosting/userscripts/raw/master/trackit-linker.user.js
// @require      https://raw.githubusercontent.com/sfable/texta/v0.0.1/lib/texta.js
// @icon         https://www.gravatar.com/avatar/0dce11eff5961aaf646345d224bf2522?r=PG&s=48&default=identicon
// @include      https://*.sydney.edu.au/*
// @include      /^https://mail\.google\.com/mail/u/0/(|#.*)$/
// @include      /^https://www\.yammer\.com/sydney\.edu\.au/(|#.*)$/
// @grant        none
// ==/UserScript==

var types = {
    "CHG": "change_request",
    "INC": "incident",
    "REQ": "u_request",
    "RITM": "sc_req_item",
    "SCREQ": "sc_request",
    "TASK": "sc_task",
    "WTASK": "u_work_task"
};

Texta("\\b("+Object.keys(types).join("|")+")\\d{7}\\b", function(m){
    var a = document.createElement("a");
    a.appendChild(document.createTextNode(m[0]));
    a.className = "trackit";
    function navurl(){
        a.setAttribute('href',"https://trackit.sydney.edu.au/"+m[0]);
    }
    navurl();
    a.addEventListener("mouseup",function(e){
        if (e.button==1) {
            a.setAttribute('href',"https://trackit.sydney.edu.au/raw/"+m[0]);
            setTimeout(navurl,50);
        }
    },true);
    return a;
}).mutate().start();
